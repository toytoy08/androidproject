import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, TouchableOpacity } from 'react-native';
import { Button } from '@ant-design/react-native'

class Page2 extends Component {

    goToPage1 = () => {
        this.props.history.push('/page1')
    }

    goToPage3 = () => {
        this.props.history.push('/page3')
    }

    goToPage4 = () => {
        this.props.history.push('/page4')
    }

    goToPage5 = () => {
        this.props.history.push('/page5')
    }

    render() {
        return (
            <View style={{ flex: 1 }}>

                {/* Header */}
                <View style={{ flex: 1, flexDirection: 'column' }}>
                    <View style={{ alignItems: 'center' }}>
                        <Text style={{ fontSize: 50 }}>Page2</Text>
                    </View>
                    <View style={{ justifyContent: "center", padding: 5 }}>
                        <Button type="primary" onPress={() => { this.goToPage1() }}>Page 1</Button>
                    </View>
                    <View style={{ justifyContent: "center", padding: 5 }}>
                        <Button type="primary" onPress={() => { this.goToPage2() }} disabled>Page 2</Button>
                    </View>
                    <View style={{ justifyContent: "center", padding: 5 }}>
                        <Button type="primary" onPress={() => { this.goToPage3() }}>Page 3</Button>
                    </View>
                    <View style={{ justifyContent: "center", padding: 5 }}>
                        <Button type="primary" onPress={() => { this.goToPage4() }}>Page 4</Button>
                    </View>
                    <View style={{ justifyContent: "center", padding: 5 }}>
                        <Button type="primary" onPress={() => { this.goToPage5() }}>Page 5</Button>
                    </View>
                </View>
                {/* Header */}

                {/* body */}
                <View>

                </View>
                {/* body */}

                {/* footer */}
                <View>

                </View>
                {/* footer */}

            </View>
        )
    }
}

export default Page2