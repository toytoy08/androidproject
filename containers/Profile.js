import React, { Component } from 'react';
import { Text, View, TouchableOpacity, Image, Alert, Modal, ScrollView } from 'react-native';
import { connect } from 'react-redux'
import { Button, Icon, InputItem } from '@ant-design/react-native'
import { push } from 'connected-react-router'
import axios from 'axios'

class Profile extends Component {

    state = {
        email: '',
        firstName: '',
        lastName: '',
        password: '',
        oldPassword: '',
        newEmail: '',
        newFirstName: '',
        newLastName: '',
        newPassword: '',
        newConfirmPassword: '',
        modalShow: false,
    }

    goToLogin = () => {
        this.props.push('/login')
    }

    goToEdit = () => {
        Alert.alert('Coming soon!!!')
    }

    UNSAFE_componentWillMount() {
        const { profile } = this.props
        console.log('token :', this.props.profile[profile.length - 1].token)

        axios({
            url: 'https://zenon.onthewifi.com/ticGo/users',
            method: 'get',
            headers: { 'Authorization': `bearer ${this.props.profile[profile.length - 1].token}` }
        })
            .then(respone => {
                const { data } = respone

                this.setState({ email: data.user.email })
                this.setState({ lastName: data.user.lastName })
                this.setState({ firstName: data.user.firstName })
                this.setState({ newFirstName: data.user.firstName })
                this.setState({ newLastName: data.user.lastName })
                // console.log(this.state.newFirstName + '&' + this.state.newLastName)
            }).catch(error => {
                console.log("ERROR:", error);
            })
    }

    isShowModal = (modalShow) => {
        // console.log('Show Modal')
        this.setState({ modalShow: !modalShow })
    }

    onClose = () => {
        this.setState({ modalShow: false })
    }

    profileSaveSystem = (newFirstName, newLastName) => {
        const { profile } = this.props
        if (newFirstName === '') {
            alert("Don't blank new firstname")
        }
        else if (newLastName === '') {
            alert("Don't blank new lastname")
        }
        else {
            // console.log('Clear condition')
            axios({
                url: 'https://zenon.onthewifi.com/ticGo/users',
                method: 'put',
                headers: { 'Authorization': `bearer ${this.props.profile[profile.length - 1].token}` },
                data: {
                    firstName: newFirstName,
                    lastName: newLastName
                }
            }).then(respone => {
                const { data } = respone
                const { user } = data
                console.log("Respone :", respone);

                axios({
                    url: 'https://zenon.onthewifi.com/ticGo/users',
                    method: 'get',
                    headers: { 'Authorization': `bearer ${this.props.profile[profile.length - 1].token}` }
                })
                    .then(respone => {
                        const { data } = respone

                        this.setState({ lastName: data.user.lastName })
                        this.setState({ firstName: data.user.firstName })
                    }).catch(error => {
                        console.log("ERROR:", error);
                        console.log('Put done')
                    })
                alert('Save All DONE!!!')
            }).catch(error => {
                console.log("ERROR:", error);
            })
        }


    }

    passwordSystem = (oldPassword, newPassword, newConfirmPassword) => {
        const { profile } = this.props
        if (oldPassword === '') {
            alert("Don't blank old password")
        }
        else if (oldPassword === 'is invalid') {
            alert('Your old password wrong')
        }
        else if (newPassword === '') {
            alert("Don't blank new password")
        }
        else if (newConfirmPassword === '') {
            alert("Don't blank new confirm password")
        }
        else if (newConfirmPassword !== newPassword) {
            alert('Your confirm password wrong')
        }
        else {
            axios({
                url: 'https://zenon.onthewifi.com/ticGo/users/password',
                method: 'put',
                headers: { 'Authorization': `bearer ${this.props.profile[profile.length - 1].token}` },
                data: {
                    oldPassword: oldPassword,
                    newPassword: newPassword
                }
            })
                .then(respone => {
                    const { data } = respone
                    const { user } = data
                    console.log("Respone :", respone);
                    alert('Set new password DONE!!!')
                }).catch(error => {
                    console.log("ERROR:", error);
                })
        }
    }

    render() {
        const { profile } = this.props
        return (
            <View style={{ flex: 1 }} >

                {/* Header */}
                < View >
                    <View style={{ alignItems: 'center', backgroundColor: '#920000' }}>
                        <Text style={{ fontSize: 50, color: 'white' }}>Profile</Text>
                    </View>
                </View >
                {/* Header */}

                {/* body */}
                < View style={{ flex: 1, marginLeft: 10 }}>
                    <ScrollView>
                        <View style={{ alignItems: 'center', flex: 1 }}>
                            <Image style={{ width: 250, height: 250, borderRadius: 250 / 2 }} source={{ uri: 'https://i.imgur.com/xoFYkyq.png' }} />
                        </View>
                        <View style={{ flex: 1 }}>
                            <View>
                                <Text style={{ fontSize: 40 }}><Icon name='mail' /> Email : </Text>
                            </View>
                            <View>
                                <Text style={{ fontSize: 30, fontWeight: 'bold' }}>{this.state.email}</Text>
                            </View>
                            <View >
                                <Text style={{ fontSize: 40 }}><Icon name='user' /> Name :</Text>
                            </View>
                            <View>
                                <Text style={{ fontSize: 30, fontWeight: 'bold' }}>{this.state.firstName + ' ' + this.state.lastName}</Text>
                            </View>
                        </View>
                    </ScrollView>
                </View >
                {/* body */}

                {/* footer */}
                <View style={{ padding: 5, marginBottom: 20 }}>
                    <View style={{ padding: 5 }}>
                        <Button type='primary' onPress={() => { this.isShowModal(this.state.modalShow) }}><Icon name='edit' /> Edit </Button>
                    </View>
                    <View style={{ padding: 5 }}>
                        <Button type='warning' onPress={() => { this.goToLogin() }}><Icon name='logout' /> Log out </Button>
                    </View>
                </View>
                {/* footer */}


                <Modal
                    visible={this.state.modalShow}
                    animationType='fade'
                >

                    <View style={{ flex: 1 }}>
                        <View>
                            <View style={{ alignItems: 'center', backgroundColor: '#920000' }}>
                                <Text style={{ fontSize: 50, color: 'white' }}>Edit Profile</Text>
                            </View>
                        </View>
                        <ScrollView>
                            <View style={{ flex: 1 }}>
                                <View style={{ marginLeft: 10 }}>
                                    <View>
                                        <Text style={{ fontSize: 40 }}>First Name :</Text>
                                    </View>
                                    <View>
                                        <InputItem
                                            defaultValue={this.state.firstName}
                                            placeholder='Firstname'
                                            onChangeText={(value) => { this.setState({ newFirstName: value }) }}
                                        />
                                    </View>

                                    <View>
                                        <Text style={{ fontSize: 40 }}>Last Name :</Text>
                                    </View>
                                    <View>
                                        <InputItem
                                            defaultValue={this.state.lastName}
                                            placeholder='Lastname'
                                            onChangeText={(value) => { this.setState({ newLastName: value }) }}
                                        />
                                    </View>
                                </View>
                                <View style={{ padding: 5 }}>
                                    <Button type='primary' onPress={() => { this.profileSaveSystem(this.state.newFirstName, this.state.newLastName) }}><Icon name='save' /> Save Name </Button>
                                </View>

                                <View style={{ marginLeft: 10 }}>
                                    <View>
                                        <Text style={{ fontSize: 40 }}>Old Password :</Text>
                                    </View>
                                    <View>
                                        <InputItem
                                            defaultValue={this.state.password}
                                            placeholder='Old Password'
                                            type='password'
                                            onChangeText={(value) => { this.setState({ oldPassword: value }) }}
                                        />
                                    </View>

                                    <View>
                                        <Text style={{ fontSize: 40 }}>New Password :</Text>
                                    </View>
                                    <View>
                                        <InputItem
                                            placeholder='New Password'
                                            type='password'
                                            onChangeText={(value) => { this.setState({ newPassword: value }) }}
                                        />
                                    </View>
                                    <View>
                                        <InputItem
                                            placeholder='New Confirm Password'
                                            type='password'
                                            onChangeText={(value) => { this.setState({ newConfirmPassword: value }) }}
                                        />
                                    </View>
                                </View>

                                <View style={{ padding: 5 }}>
                                    <Button type='primary' onPress={() => { this.passwordSystem(this.state.oldPassword, this.state.newPassword, this.state.newConfirmPassword) }}><Icon name='edit' /> Set new password </Button>
                                </View>

                            </View>
                        </ScrollView>

                        <View style={{ marginBottom: 20, padding: 5 }}>
                            <View>
                                <Button type='primary' onPress={() => { this.onClose() }}><Icon name='close' /> Close </Button>
                            </View>

                        </View>

                    </View>

                </Modal>

            </View >


        )
    }
}

const mapStateToProps = (state) => {
    return {
        profile: state.profile
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        push: location => {
            dispatch(push(location))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile)