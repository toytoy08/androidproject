import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, TouchableOpacity, Alert } from 'react-native';
import { Button, InputItem, Icon } from '@ant-design/react-native'
import { connectRouter } from 'connected-react-router';
import { connect } from 'react-redux'
import axios from 'axios'

class Register extends Component {

    state = {
        firstName: '',
        lastName: '',
        email: '',
        password: '',
        confirmPassword: ''
    }

    RegisterSystem = (email, password, firstName, lastName) => {
        axios({
            url: 'https://zenon.onthewifi.com/ticGo/users/register',
            method: 'post',
            data: {
                email: email,
                password: password,
                firstName: firstName,
                lastName: lastName
            }
        })
            .then(res => {
                const { data } = res
                const { user } = data
                this.props.history.push('/login')
            })
            .catch(e => {
                if (e.response.data.errors.email === "duplicated email") {
                    alert('This email has already been used.')
                }
            })
    }

    RegisLogic = (firstName, lastName, email, password, confirmPassword) => {
        if (firstName === '') {
            alert("Don't blank firstname")
        }
        else if (lastName === '') {
            alert("Don't blank lastname")
        }
        else if (email === '') {
            alert("Don't blank email")
        }
        else if (password === '') {
            alert("Don't blank password")
        }
        else if (confirmPassword !== password) {
            alert("Password and Password confirm doesn't match")
        }
        else {
            this.RegisterSystem(email, password, firstName, lastName)
        }
    }

    goToLogin = (email, password) => {
        if (this.validateEmail(email) && this.validatePassword(password) == true) {
            this.addProfile
            console.log(this.addProfile)
            Alert.alert('Register Success','Try to use new account')
            this.props.history.push('/login')
        }
        else if (this.state == null) {
            Alert.alert('Please insert information', 'try not to blank input box')
        }
        else {
            console.log(this.state);

            Alert.alert('Your email or password wrong condition!!!', 'check condition before confirm')
        }
    }

    cancelRegister = () => {
        this.props.history.push('/login')
    }

    validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    validatePassword(password) {
        var re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;
        return re.test(String(password));
    }

    render() {
        const { email, password } = this.props
        return (
            <View style={{ flex: 1 }} >

                {/* Header */}
                < View style={{}}>
                    <View style={{ alignItems: 'center', backgroundColor: '#795548' }}>
                        <Text style={{ fontSize: 50, color: 'white' }}>Register Page</Text>
                    </View>

                </View >
                {/* Header */}

                {/* body */}
                <View style={{ flex: 1, justifyContent: 'center' }}>
                    <View style={{ justifyContent: "center", padding: 5 }}>

                        <ScrollView>
                            <InputItem
                                clear
                                placeholder="firstname"
                                onChangeText={(value) => { this.setState({ firstName: value }) }}
                            />
                            <InputItem
                                clear
                                placeholder="lastname"
                                onChangeText={(value) => { this.setState({ lastName: value }) }}
                            />
                            <InputItem
                                clear
                                placeholder="username@email.com"
                                onChangeText={(value) => { this.setState({ email: value }) }}
                            />
                            <InputItem
                                clear
                                type="password"
                                placeholder="password"
                                onChangeText={(value) => { this.setState({ password: value }) }}
                            />
                            <InputItem
                                clear
                                type="password"
                                placeholder="confirm password"
                                onChangeText={(value) => { this.setState({ confirmPassword: value }) }}
                            />
                        </ScrollView>

                    </View>

                </View>
                {/* body */}

                {/* footer */}
                <View style={{ marginBottom: 20, padding: 5 }}>
                    <View style={{ justifyContent: "center", padding: 5 }}>
                        <Button
                            type="primary"
                            onPress={() => {
                                this.RegisLogic(
                                    this.state.firstName,
                                    this.state.lastName,
                                    this.state.email,
                                    this.state.password,
                                    this.state.confirmPassword
                                )
                            }} >
                            Confirm <Icon name='check' />
                        </Button>
                    </View>
                    <View style={{ justifyContent: "center", padding: 5 }}>
                        <Button
                            type="primary"
                            onPress={() => {
                                this.cancelRegister()
                            }} >
                            Cancel <Icon name='home' />
                        </Button>
                    </View>
                </View>
                {/* footer */}

            </View >
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addProfile: (email, password, firstname, lastname, token) => {
            dispatch({
                type: 'ADD_PROFILE',
                email: email,
                password: password,
                firstname: firstname,
                lastname: lastname,
                token: token
            })
        }
    }
}

export default connect(null, mapDispatchToProps)(Register)