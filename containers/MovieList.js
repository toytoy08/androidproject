import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, TouchableOpacity, Alert, FlatList, Modal, Image } from 'react-native';
import { Button, Card, WhiteSpace, SearchBar, WingBlank, Icon, List } from '@ant-design/react-native'
import axios from 'axios'
import { connect } from 'react-redux'
import { push } from 'connected-react-router'

var options = { weekday: 'short', month: 'short', day: 'numeric' }
var dateAll = Date.now()
// console.log(dateAll)
var date2 = new Date(new Date().getTime() + 24 * 60 * 60 * 1000)
date2 = date2.getTime()
var date3 = new Date(new Date().getTime() + 48 * 60 * 60 * 1000)
date3 = date3.getTime()

const Item = List.Item

class MovieList extends Component {

    state = {
        movies: [],
        movie: [],
        movieModal1: [],
        movieModal2: [],
        movieModal3: [],
        modalShow: false,
        dateTime1: null,
        dateTime2: null,
        dateTime3: null,
        dateCount: 0,
    }

    goToBooking = (item) => {
        // console.log(item)
        // console.log(this.state.movie)
        // console.log(this.state.movies)
        // this.setState({ movie: item })
        this.props.push('/booking', { movie: item })
    }

    UNSAFE_componentWillMount() {
        axios.get('https://zenon.onthewifi.com/ticGo/movies')
            .then(response => response.data)
            .then(data => {
                this.setState({ movies: data })
                this.setState({ dateTime1: dateAll })
                this.setState({ dateTime2: date2 })
                this.setState({ dateTime3: date3 })
            })
    }

    fillZero = (time) => {
        if (time < 10) {
            time = "0" + time
        }
        return time
    }

    isShowModal = (movie, movieId) => {
        console.log(movie)
        this.setState({
            modalShow: true,
            movie: movie
        })
        this.getDate1(movieId)
        this.getDate2(movieId)
        this.getDate3(movieId)
    }

    getDate1 = (movieId) => {
        axios.get(`https://zenon.onthewifi.com/ticGo/movies/showtimes/movie/${movieId}`, {
            params: { date: this.state.dateTime1 }
        })
            .then(response => response.data)
            .then(data => {
                this.setState({ movieModal1: data })
                console.log(data)
            })
    }

    getDate2 = (movieId) => {
        axios.get(`https://zenon.onthewifi.com/ticGo/movies/showtimes/movie/${movieId}`, {
            params: { date: this.state.dateTime2 }
        })
            .then(response => response.data)
            .then(data => {
                this.setState({ movieModal2: data })
                console.log(data)
            })
    }

    getDate3 = (movieId) => {
        axios.get(`https://zenon.onthewifi.com/ticGo/movies/showtimes/movie/${movieId}`, {
            params: { date: this.state.dateTime3 }
        })
            .then(response => response.data)
            .then(data => {
                this.setState({ movieModal3: data })
                console.log(data)
            })
    }

    onClose = () => {
        this.setState({ modalShow: false })
    }

    render() {
        // console.log(this.state.movies);
        // console.log(this.loopDate(date))
        return (
            <View style={{ flex: 1 }} >

                {/* Header */}
                < View >
                    <View style={{ alignItems: 'center', backgroundColor: '#009688' }}>
                        <Text style={{ fontSize: 50, color: 'white' }}>Movie List</Text>
                    </View>
                </View >
                {/* Header */}

                {/* body */}
                < View style={{ flex: 1 }}>
                    <FlatList
                        data={this.state.movies}
                        renderItem={({ item }) => {
                            // console.log('Item :', item);
                            return <TouchableOpacity onPress={() => { this.isShowModal(item, item._id) }}>
                                <Card>
                                    <Card.Header
                                        title={<Text style={{ fontSize: 30, color: 'black' }}>{item.name}</Text>}
                                        thumbStyle={{ width: 130, height: 192 }}
                                        thumb={item.image}
                                    />

                                    <Card.Body style={{ alignItems: 'center', backgroundColor: 'blue', borderWidth: 3, borderColor: 'gray' }}>
                                        <Text style={{ color: 'white', fontSize: 30 }}>
                                            Duration :  {item.duration}  min
                                        </Text>
                                    </Card.Body>

                                </Card>
                            </TouchableOpacity>
                        }}
                    />
                </View >
                {/* body */}

                {/* footer */}
                <View>

                </View>
                {/* footer */}

                <Modal
                    visible={this.state.modalShow}
                    animationType='fade'
                >
                    <View style={{ flex: 1 }}>

                        {/* Header */}
                        <View>
                            <View style={{ alignItems: 'center', backgroundColor: '#009688' }}>
                                <Text style={{ fontSize: 50, color: 'white' }}>Movie Detail</Text>
                            </View>
                        </View>
                        {/* Header */}

                        {/* body */}
                        <ScrollView>
                            <View style={{ flex: 1 }}>

                                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                    <Image style={{ height: 608, width: 342 }} source={{ uri: this.state.movie.image }} />
                                </View>
                                <View style={{ alignItems: 'center' }}>
                                    <Text style={{ fontSize: 30 }}>{this.state.movie.name}</Text>
                                </View>
                                <View style={{ alignItems: 'center' }}>
                                    <Text style={{ fontSize: 30 }}>Duration : {this.state.movie.duration} min</Text>
                                </View>

                                <View style={{ flex: 1 }}>
                                    {/* Showtime Today */}
                                    <View style={{ alignItems: 'center' }}>
                                        <Text style={{ fontSize: 30, fontWeight: 'bold' }}>showtimes {new Date(this.state.dateTime1).toLocaleDateString("th-TH", options)}</Text>
                                    </View>

                                    <List renderHeader={'Showtimes'}>
                                        <FlatList
                                            data={this.state.movieModal1}
                                            renderItem={({ item }) =>
                                                <Item onPress={() => { this.goToBooking(item) }}>
                                                    <Text style={{ fontSize: 20 }}>
                                                        {this.fillZero(new Date(item.startDateTime).getHours()) + ' : ' + this.fillZero(new Date(item.startDateTime).getMinutes())}
                                                    </Text>
                                                </Item>
                                            }
                                        />
                                    </List>

                                    {/* Showtime Today */}


                                    {/* Showtime tomorrow */}
                                    <View style={{ alignItems: 'center' }}>
                                        <Text style={{ fontSize: 30, fontWeight: 'bold' }}>showtimes {new Date(this.state.dateTime2).toLocaleDateString("th-TH", options)}</Text>
                                    </View>

                                    <List renderHeader={'Showtimes'}>
                                        <FlatList
                                            data={this.state.movieModal2}
                                            renderItem={({ item }) =>
                                                <Item onPress={() => { this.goToBooking(item) }}>
                                                    <Text style={{ fontSize: 20 }}>
                                                        {this.fillZero(new Date(item.startDateTime).getHours()) + ' : ' + this.fillZero(new Date(item.startDateTime).getMinutes())}
                                                    </Text>
                                                </Item>
                                            }
                                        />
                                    </List>
                                    {/* Showtime tomorrow */}

                                    {/* Showtime after tomorrow */}
                                    <View style={{ alignItems: 'center' }}>
                                        <Text style={{ fontSize: 30, fontWeight: 'bold' }}>showtimes {new Date(this.state.dateTime3).toLocaleDateString("th-TH", options)}</Text>
                                    </View>

                                    <List renderHeader={'Showtimes'}>
                                        <FlatList
                                            data={this.state.movieModal3}
                                            renderItem={({ item }) =>
                                                <Item onPress={() => { this.goToBooking(item) }}>
                                                    <Text style={{ fontSize: 20 }}>
                                                        {this.fillZero(new Date(item.startDateTime).getHours()) + ' : ' + this.fillZero(new Date(item.startDateTime).getMinutes())}
                                                    </Text>
                                                </Item>
                                            }
                                        />
                                    </List>
                                    {/* Showtime after tomorrow */}

                                </View>

                            </View>
                        </ScrollView>
                        {/* body */}

                        {/* footer */}
                        <View>
                            <View style={{ padding: 5 }}>
                                <Button type='primary' onPress={() => { this.onClose() }}>
                                    <Icon name='close' /> Close
                                 </Button>
                            </View>
                        </View>
                        {/* footer */}

                    </View>
                </Modal>

            </View >
        )
    }
}

export default connect(null, { push })(MovieList)