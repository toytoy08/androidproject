import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, TouchableOpacity, Image } from 'react-native';
import { Button } from '@ant-design/react-native'
import { push } from 'connected-react-router'
import { connect } from 'react-redux'
import axios from 'axios'

var options = { weekday: 'short', month: 'short', day: 'numeric' }

class Booking extends Component {

    state = {
        cinema: [],
        startDateTime: [],
        endDateTime: [],
        movie: [],
        // sofa: [],
        // premium: [],
        // deluxe: [],
    }

    fillZero = (time) => {
        if (time < 10) {
            time = "0" + time
        }
        return time
    }

    goToConfirm = () => {
        const { profile } = this.props
        console.log(this.state.movie._id)
        axios({
            method: 'post',
            url: 'https://zenon.onthewifi.com/ticGo/movies/book',
            headers: { 'Authorization': `bearer ${this.props.profile[profile.length - 1].token}` },
            data: {
                showtimeId: this.state.movie._id,
                seats: [
                    {
                        type: "DELUXE",
                        row: "8",
                        column: "13"
                    }
                ]
            }
        })
            .then(response => response.data)
            .then(() => {
                this.props.push('/page4', { index: 0 })
                alert('Booking Done!!!')
            })
            .catch((error) => {
                console.log(error);
                alert(error)
            })
    }

    goToCancel = () => {
        this.props.push('/page4', { index: 0 })
    }

    UNSAFE_componentWillMount() {
        console.log('Prop movie:', this.props.location)
        this.setState({
            cinema: this.props.location.state.movie.cinema,
            movie: this.props.location.state.movie,
            // sofa: this.props.location.state.movie.seats[0],
            // premium: this.props.location.state.movie.seats[1],
            // deluxe: this.props.location.state.movie.seats[2],
            startDateTime: this.props.location.state.movie.startDateTime,
            endDateTime: this.props.location.state.movie.endDateTime,
        })

    }

    render() {
        // console.log('Props :', this.props)
        return (
            <View style={{ flex: 1 }}>

                {/* Header */}
                <View>
                    <View style={{ alignItems: 'center', backgroundColor: 'black' }}>
                        <Text style={{ fontSize: 50, color: 'white' }}>Booking</Text>
                    </View>
                </View>
                {/* Header */}

                {/* body */}
                <View style={{ flex: 1, padding: 5 }}>
                    <ScrollView>
                        <View style={{ alignItems: 'center', padding: 5 }}>
                            <Image style={{ width: 171, height: 304 }} source={{ uri: this.state.movie.movie.image }} />
                        </View>
                        <View style={{ padding: 5, borderWidth: 4, flexDirection: 'row' }}>

                            <View style={{ flexDirection: 'column' }}>
                                <View>
                                    <Text style={{ fontSize: 20 }}>Name :</Text>
                                </View>
                                <View>
                                    <Text style={{ fontSize: 20 }}>Duration : </Text>
                                </View>
                                <View>
                                    <Text style={{ fontSize: 20 }}>Time : </Text>
                                </View>
                                <View>
                                    <Text style={{ fontSize: 20 }}>Soundtrack : </Text>
                                </View>
                                <View>
                                    <Text style={{ fontSize: 20 }}>Subtitle : </Text>
                                </View>
                            </View>

                            <View style={{ flexDirection: 'column' }}>
                                <View>
                                    <Text style={{ fontSize: 20 }}>{this.state.movie.movie.name}</Text>
                                </View>
                                <View>
                                    <Text style={{ fontSize: 20 }}>{this.state.movie.movie.duration} min</Text>
                                </View>
                                <View>
                                    <Text style={{ fontSize: 20 }}>{this.fillZero(new Date(this.state.movie.startDateTime).getHours())}:{this.fillZero(new Date(this.state.movie.startDateTime).getMinutes())} - {this.fillZero(new Date(this.state.movie.endDateTime).getHours())}:{this.fillZero(new Date(this.state.movie.endDateTime).getMinutes())}</Text>
                                </View>
                                <View>
                                    <Text style={{ fontSize: 20 }}>{this.state.movie.soundtrack}</Text>
                                </View>
                                <View>
                                    <Text style={{ fontSize: 20 }}>{this.state.movie.subtitle}</Text>
                                </View>
                            </View>

                        </View>
                    </ScrollView>
                </View>
                {/* body */}

                {/* footer */}
                <View>

                    <View style={{ padding: 5 }}>
                        <Button type='primary' onPress={() => { this.goToConfirm() }}>Confirm</Button>
                    </View>
                    <View style={{ padding: 5 }}>
                        <Button type='primary' onPress={() => { this.goToCancel() }}>Cancel</Button>
                    </View>
                </View>
                {/* footer */}

            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        profile: state.profile
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        push: location => {
            dispatch(push(location))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Booking)