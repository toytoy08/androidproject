import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, TouchableOpacity, FlatList } from 'react-native';
import { Button } from '@ant-design/react-native'
import { push } from 'connected-react-router'
import { connect } from 'react-redux'
import axios from 'axios'

var options = { weekday: 'short', month: 'short', day: 'numeric' }

class History extends Component {

    state = {
        bookingDetail: [],
        firstName: '',
        lastName: '',
        movie: [],
        movieName:[],
    }

    UNSAFE_componentWillMount() {
        const { profile } = this.props
        axios({
            url: 'https://zenon.onthewifi.com/ticGo/users',
            method: 'get',
            headers: { 'Authorization': `bearer ${this.props.profile[profile.length - 1].token}` }
        })
            .then(respone => {
                const { data } = respone

                this.setState({ lastName: data.user.lastName })
                this.setState({ firstName: data.user.firstName })
                this.getBooking()
            }).catch(error => {
                console.log("ERROR:", error);
            })
    }

    getBooking = () => {
        const { profile } = this.props
        console.log(profile)
        axios({
            url: `https://zenon.onthewifi.com/ticGo/movies/book`,
            method: 'get',
            headers: { 'Authorization': `bearer ${this.props.profile[profile.length - 1].token}` },
        })
            .then(respone => respone.data)
            .then(data => {
                this.setState({ bookingDetail: data })
                console.log('Booking Detail : ', this.state.bookingDetail)
                for (let loop = 0; loop < data.length; loop++) {
                    console.log('Booking Showtime : ', this.state.bookingDetail[0].showtime)
                    this.getShowtime(this.state.bookingDetail[loop].showtime)
                }
            })
    }

    getShowtime = (showtime) => {
        axios({
            url: `https://zenon.onthewifi.com/ticGo/movies/showtimes/${showtime}`,
            method: 'get'
        })
            .then(response => response.data)
            .then(data => {
                this.setState({ movie: data })
                console.log('Movie name:', this.state.movie.movie.name)
            })
    }

    fillZero = (time) => {
        if (time < 10) {
            time = "0" + time
        }
        return time
    }

    render() {
        const { profile } = this.props
        return (
            <View style={{ flex: 1 }} >

                {/* Header */}
                < View >
                    <View style={{ alignItems: 'center', backgroundColor: '#795548' }}>
                        <Text style={{ fontSize: 50, color: 'white' }}>History</Text>
                    </View>
                </View >
                {/* Header */}

                {/* body */}
                <View style={{ flex: 1, justifyContent: 'center' }}>
                    <View>
                        <Button onPress={() => { this.getBooking() }}>Log Data</Button>
                    </View>
                    <ScrollView>
                        <View>
                            <FlatList
                                inverted
                                data={this.state.bookingDetail}
                                renderItem={({ item }) =>
                                    <View>
                                        {/* <Text style={{ fontSize: 20 }}>No. {index}</Text> */}
                                        {/* {this.getShowtime(item.showtime)} */}
                                        <Text style={{ fontSize: 20 }}>CreatedDateTime : {new Date(item.createdDateTime).toLocaleDateString("th-TH", options) + ' ' + this.fillZero(new Date(item.createdDateTime).getHours()) + ' : ' + this.fillZero(new Date(item.createdDateTime).getMinutes())} </Text>
                                        <Text style={{ fontSize: 20 }}>User : {this.state.firstName} {this.state.lastName}</Text>
                                        {/* <Text style={{ fontSize: 20 }}>Movie :  {this.state.movie.movie.name} </Text> */}
                                        <Text></Text>
                                    </View>
                                }
                            />
                        </View>
                    </ScrollView>
                </View>
                {/* body */}

                {/* footer */}
                <View>
                    <View>

                        {/* <Button type='primary' onPress={() => { }}>Cancel</Button> */}
                    </View>
                </View>
                {/* footer */}

            </View >
        )
    }
}

const mapStateToProps = (state) => {
    return {
        profile: state.profile
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        push: location => {
            dispatch(push(location))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(History)