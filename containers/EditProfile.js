import React, { Component } from 'react';
import { Text, View, TouchableOpacity, Image, Alert } from 'react-native';
import { connect } from 'react-redux'
import { Button, Icon, InputItem } from '@ant-design/react-native'
import { push } from 'connected-react-router'
import axios from 'axios'

class EditProfile extends Component {

    state = {
        email: '',
        firstName: '',
        lastName: '',
    }

    saveProfile = () => {
        alert('All Done')
        this.props.push('/login')
    }

    UNSAFE_componentWillMount() {
        const { profile } = this.props
        // console.log('token :', this.props.profile[profile.length - 1].token)

        axios({
            url: 'https://zenon.onthewifi.com/ticGo/users',
            method: 'get',
            headers: { 'Authorization': `bearer ${this.props.profile[profile.length - 1].token}` }
        })
            .then(respone => {
                const { data } = respone

                this.setState({ email: data.user.email })
                this.setState({ lastName: data.user.lastName })
                this.setState({ firstName: data.user.firstName })
            }).catch(error => {
                console.log("ERROR:", error);
            })
    }

    render() {
        const { profile } = this.props
        return (
            <View style={{ flex: 1 }} >

                {/* Header */}
                < View >
                    <View style={{ alignItems: 'center', backgroundColor: '#920000' }}>
                        <Text style={{ fontSize: 50, color: 'white' }}>Profile</Text>
                    </View>
                </View >
                {/* Header */}

                {/* body */}
                < View style={{ flex: 1, justifyContent: 'center' }}>
                    <View style={{ alignItems: 'center' }}>
                        <Image source={{ uri: 'https://www.almau.edu.kz/img/no_image.png' }} />
                    </View>
                    <View style={{ marginLeft: 10 }}>
                        <Text style={{ fontSize: 40 }}><Icon name='mail' /> Email : </Text>
                    </View>
                    <View>
                        <InputItem style={{ fontSize: 30, fontWeight: 'bold' }}>{this.state.email}</InputItem>
                    </View>
                    <View>
                        <Text style={{ fontSize: 40 }}>First Name :</Text>
                    </View>
                    <View>
                        <InputItem style={{ fontSize: 30, fontWeight: 'bold' }}>{this.state.firstName}</InputItem>
                    </View>
                    <View>
                        <Text style={{ fontSize: 40 }}>Last Name :</Text>
                    </View>
                    <View>
                        <InputItem style={{ fontSize: 30, fontWeight: 'bold' }}>{this.state.lastName}</InputItem>
                    </View>
                </View >
                {/* body */}

                {/* footer */}
                <View style={{ padding: 5, marginBottom: 20 }}>
                    <View style={{ padding: 5 }}>
                        <Button type='primary' onPress={() => { this.saveProfile() }}><Icon name='edit' /> Save Change </Button>
                    </View>
                </View>
                {/* footer */}

            </View >
        )
    }
}

const mapStateToProps = (state) => {
    return {
        profile: state.profile
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        push: location => {
            dispatch(push(location))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditProfile)