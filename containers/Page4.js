import React, { Component } from 'react';
import { BottomNavigation, Text } from 'react-native-paper';
import Main from './Main'
import MovieList from './MovieList'
import History from './History'
import Profile from './Profile'

const MovieListRoute = () => <MovieList />

const HistoryRoute = () => <History />

const ProfileRoute = () => <Profile />

const MainRoute = () => <Main />

class Page4 extends Component {

    state = {
        index: 0,
        routes: [
            { key: 'movielist', title: 'MovieList', icon: 'movie', color: '#009688' },
            { key: 'history', title: 'History', icon: 'history', color: '#795548' },
            { key: 'profile', title: 'Profile', icon: 'person', color: '#920000' },
            { key: 'about', title: 'About App', icon: 'help', color: '#00A109' },
        ],
    };

    _handleIndexChange = index => this.setState({ index });

    // _renderScene = BottomNavigation.SceneMap({
    //     music: MusicRoute,
    //     albums: AlbumsRoute,
    //     recents: RecentsRoute,
    //     purchased: PurchasedRoute
    // });

    _renderScene = ({ route, jumpTo }) => {
        switch (route.key) {
            case 'about':
                return <MainRoute jumpTo={jumpTo} />
            case 'movielist':
                return <MovieListRoute jumpTo={jumpTo} />
            case 'history':
                return <HistoryRoute jumpTo={jumpTo} />
            case 'profile':
                return <ProfileRoute jumpTo={jumpTo} />
        }
    }

    render() {
        return (
            <BottomNavigation
                navigationState={this.state}
                onIndexChange={this._handleIndexChange}
                renderScene={this._renderScene}
            />
        )
    }
}

export default Page4