import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, TouchableOpacity, Alert } from 'react-native';
import { Button } from '@ant-design/react-native'

class Main extends Component {

    Something = () => {
        Alert.alert('Nothing here', 'But i want to test this button')
    }

    render() {
        return (
            <View style={{ flex: 1 }}>

                {/* Header */}
                <View>
                    <View style={{ alignItems: 'center', backgroundColor: '#00A109', }}>
                        <Text style={{ fontSize: 50, color: 'white' }}>About ticGo</Text>
                    </View>
                </View>
                {/* Header */}

                {/* body */}
                <View style={{ flex: 1 }}>
                    <View style={{ justifyContent: 'center' }}>
                        <Text style={{ fontSize: 40 }}>    This application can check list movie and can book in app</Text>
                    </View>
                    <View style={{ justifyContent: 'center' }}>
                        <Text style={{ fontSize: 25 }}>    This menu create for make bottom navigation to default of bottom navigation
                                                            if delete 1 menu of this then the remain 3 menus got bug color they not change color</Text>
                    </View>
                    <View style={{ justifyContent: 'center' }}>
                        <Text style={{ fontSize: 25,color:'black' }}> If this app not finish the only one i can say is " Sorry "</Text>
                    </View>
                </View>
                {/* body */}

                {/* footer */}
                <View>
                    <View>
                        <Button type='ghost' onPress={this.Something}>Something here</Button>
                    </View>
                </View>
                {/* footer */}

            </View>
        )
    }
}

export default Main