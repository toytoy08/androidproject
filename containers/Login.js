import React, { Component } from 'react';
import { StyleSheet, Alert, Text, View, ScrollView, TouchableOpacity, Image } from 'react-native';
import { Button, InputItem, Icon } from '@ant-design/react-native'
import { connect } from 'react-redux'
import axios from 'axios'

class Login extends Component {

    state = {
        email: '',
        password: ''
    };

    loginSystem = (email, password) => {
        axios({
            url: 'https://zenon.onthewifi.com/ticGo/users/login?email',
            method: 'post',
            data: {
                email: email,
                password: password,
            }
        }).then(response => {
            const { data } = response
            this.props.addProfile(data.user.email, data.user.password, data.user.firstName, data.user.lastName, data.user.token)
            this.props.history.push('/page4')
        }).catch(err => {
            if (err.response.data.errors.email === "Email can't be blank") {
                alert(err.response.data.errors.email)
            } else if (err.response.data.errors.password === "Password can't be blank") {
                alert(err.response.data.errors.password)
            }
            console.log("error " + err.response.data.errors.email)
            console.log("error " + err.response.data.errors.password)
        })
    }

    goToMain = (email, password) => {
        if (this.validateEmail(email) && this.validatePassword(password) == true) {
            this.props.history.push('/page4')
        }
        else if ((this.state.email && this.state.password) === '') {
            Alert.alert("Don't Blank", "Blank all")
        }
        else {
            console.log(this.state)
            Alert.alert('Your email or password wrong!!!', 'Try to remember your account')
        }
    }

    goPass = () => {
        axios({
            url: 'https://zenon.onthewifi.com/ticGo/users/login?email',
            method: 'post',
            data: {
                email: 'asd123@gmail.com',
                password: 'asd123',
            }
        }).then(response => {
            const { data } = response
            this.props.addProfile(data.user.email, data.user.password, data.user.firstName, data.user.lastName, data.user.token)
            this.props.history.push('/page4')
        }).catch(err => {
            if (err.response.data.errors.email === "Email can't be blank") {
                alert(err.response.data.errors.email)
            } else if (err.response.data.errors.password === "Password can't be blank") {
                alert(err.response.data.errors.password)
            }
            console.log("error " + err.response.data.errors.email)
            console.log("error " + err.response.data.errors.password)
        })
    }

    goToRegister = () => {
        this.props.history.push('/register')
    }

    validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    validatePassword(password) {
        var re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;
        return re.test(String(password));
    }

    render() {
        return (

            <View style={{ flex: 1 }}>

                {/* Header */}
                <View style={{ flex: 2, justifyContent: 'center' }}>
                    {/* <View style={{ alignItems: 'center' }}>
                        <Text style={{ fontSize: 50 }}>ticGo</Text>
                    </View> */}
                    <View style={{ alignItems: 'center' }}>
                        <Image style={{ width: 250, height: 250, borderRadius: 250 / 2 }} source={{ uri: 'https://i.imgur.com/xoFYkyq.png' }} />
                    </View>

                </View>
                {/* Header */}

                {/* body */}
                <View style={{ flex: 1 }}>
                    <View style={{ justifyContent: "center", padding: 5, flex: 1 }}>

                        <InputItem
                            clear
                            placeholder="username@email.com"
                            onChangeText={(value) => { this.setState({ email: value }) }}
                        >
                            <Icon name='mail' />
                        </InputItem>
                        <InputItem
                            clear
                            type="password"
                            placeholder="password"
                            onChangeText={(value) => { this.setState({ password: value }) }}
                        >
                            <Icon name='lock' />
                        </InputItem>
                    </View>

                </View>
                {/* body */}

                {/* footer */}
                <View style={{ marginBottom: 20, padding: 5 }}>
                    <View style={{ justifyContent: "center", padding: 5 }}>
                        <Button type="primary" onPress={() => { this.goPass() }}><Icon name='unlock' style={{}} /> Pass all </Button>
                    </View>
                    <View style={{ justifyContent: "center", padding: 5 }}>
                        <Button type="primary" onPress={() => { this.loginSystem(this.state.email, this.state.password) }}><Icon name='login' /> Login </Button>
                    </View>
                    <View style={{ justifyContent: "center", padding: 5 }}>
                        <Button type="primary" onPress={() => { this.goToRegister() }}><Icon name='user-add' /> Don't have any account? </Button>
                    </View>
                </View>
                {/* footer */}

            </View>

        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addProfile: (email, password, firstname, lastname, token) => {
            dispatch({
                type: 'ADD_PROFILE',
                email: email,
                password: password,
                firstname: firstname,
                lastname: lastname,
                token: token
            })
        }
    }
}

export default connect(null, mapDispatchToProps)(Login)