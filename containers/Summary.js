import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, TouchableOpacity } from 'react-native';
import { Button, Icon } from '@ant-design/react-native'
import { push } from 'connected-react-router'
import { connect } from 'react-redux'
import axios from 'axios'

class Summary extends Component {

    goToMain = () => {
        this.props.push('/page4')
    }

    render() {
        return (
            <View style={{ flex: 1 }}>

                {/* Header */}
                <View>
                    <View style={{ alignItems: 'center', backgroundColor: '#8227E1' }}>
                        <Text style={{ fontSize: 50, color: 'white' }}>Summary</Text>
                    </View>
                </View>
                {/* Header */}

                {/* body */}
                <View style={{ flex: 1 }}>
                    <Text></Text>
                </View>
                {/* body */}

                {/* footer */}
                <View>
                    <View style={{ padding: 5 }}>
                        <Button type='primary' onPress={() => { this.goToMain() }}>
                            <Icon name='home' /> Back to Main
                        </Button>
                    </View>
                </View>
                {/* footer */}

            </View>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        push: location => {
            dispatch(push(location))
        }
    }
}

export default connect(null, mapDispatchToProps)(Summary)