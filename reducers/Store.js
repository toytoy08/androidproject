
import { createMemoryHistory } from 'history'
import { applyMiddleware, createStore, compose, combineReducers } from 'redux'
import { routerMiddleware, connectRouter } from 'connected-react-router'
import ProfileReducer from './ProfileReducer'
import logger from 'redux-logger'

const reducers = (history) => combineReducers({
    profile: ProfileReducer,
    router: connectRouter(history)
})

export const history = createMemoryHistory()

export const store = createStore(
    reducers(history),
    compose(
        applyMiddleware(
            routerMiddleware(history),
            logger
        )
    )
)
export default store