import React, { Component } from 'react';
import { Route, NativeRouter, Switch, Redirect } from 'react-router-native'

import MainApp from './containers/Main'
import Page1 from './containers/Page1'
import Page2 from './containers/Page2'
import Page3 from './containers/Page3'
import Page4 from './containers/Page4'
import Page5 from './containers/Page5'
import Register from './containers/Register'
import Login from './containers/Login'
import Profile from './containers/Profile'
import Booking from './containers/Booking'
import Summary from './containers/Summary'

import { Provider } from 'react-redux';
import { store, history } from './reducers/Store'
import { ConnectedRouter } from 'connected-react-router';

class Router extends Component {
    render() {
        return (
            <Provider store={store}>
                <ConnectedRouter history={history}>
                    <Switch>
                        <Route exact path="/main" component={MainApp} />
                        <Route exact path="/page1" component={Page1} />
                        <Route exact path="/page2" component={Page2} />
                        <Route exact path="/page3" component={Page3} />
                        <Route exact path="/page4" component={Page4} />
                        <Route exact path="/page5" component={Page5} />
                        <Route exact path="/login" component={Login} />
                        <Route exact path="/register" component={Register} />
                        <Route exact path="/profile" component={Profile} />
                        <Route exact path="/booking" component={Booking} />
                        <Route exact path="/summary" component={Summary} />
                        <Redirect to="/login" />
                    </Switch>
                </ConnectedRouter>
            </Provider>
        )
    }
}

export default Router